# xCloud Project README

Welcome to xCloud, a project built with Next.js and Express, comprising both frontend and backend components. This project leverages Dockerode to create containers for projects hosted by users on the xCloud platform. Additionally, it utilizes PostgreSQL for data storage.

## Project Structure

The project is divided into two main parts:

1. **/backend**: This directory contains the backend code built with Express. It handles server-side logic, API endpoints, interactions with the database, and Docker container management.

2. **/frontend**: Here lies the frontend codebase, developed using Next.js. It handles the user interface, client-side logic, and interactions with the backend API.

## Setup Instructions

To set up the xCloud project locally, follow these steps:

1. **Clone Repository**: Clone this repository to your local machine.

2. **Install Dependencies**:

   - Navigate to the root directory of the project.
   - Inside the **/backend** directory, run `npm install` to install backend dependencies.
   - Inside the **/frontend** directory, run `npm install` to install frontend dependencies.

3. **Configuration**:

   - Configure your environment variables as needed for both frontend and backend components. Refer to `.env` files in respective directories for guidance.
   - Ensure you have PostgreSQL installed and configured. Update the database connection settings in the backend `.env` file accordingly.
   - Import schema.sql into PostgreSQL database named xcloud (or update usage accordingly).

4. **Running the Application**:

   - Start the PostgreSQL server.
   - Start the backend server: Inside the **/backend** directory, run `npm run dev` to start the Express server.
   - Start the frontend server: Inside the **/frontend** directory, run `npm run dev` to start the Next.js development server.

5. **Accessing the Application**:
   - Once both backend and frontend servers are running, you can access the application by visiting `http://localhost:3000` in your web browser.

## Docker Integration

xCloud utilizes Dockerode for containerization. Docker containers are created to host projects on the xCloud platform. PostgreSQL can also be containerized for development or deployment purposes.

## Contributions

Contributions to xCloud are welcome! If you find any issues or have ideas for improvements, feel free to submit a pull request or open an issue on GitHub.

Thank you for choosing xCloud for your project hosting needs! Happy coding! 🚀

**Notes:**

- This README is generated using ChatGPT and is still a work in progress.
- Make sure you have Docker and PostgreSQL installed and properly configured on your system for complete functionality.
